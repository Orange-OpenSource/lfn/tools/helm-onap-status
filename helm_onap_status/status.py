#!/usr/bin/env python
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import os
import glob
import logging
import re
import time
import subprocess
import yaml

from shutil import copyfile

from xtesting.core import testcase
from jinja2 import Environment, PackageLoader, select_autoescape

ONAP_RELEASE = os.getenv('ONAP_RELEASE', 'onap')
ONAP_HELM_LOG_PATH = os.getenv('ONAP_HELM_LOG_PATH', '/onap_helm_logs')
HELM_BIN = os.getenv('HELM_BIN', 'helm')

class Status(testcase.TestCase):
    """Retrieve status of ONAP helm deployments."""

    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Init the testcase."""
        if "case_name" not in kwargs:
            kwargs["case_name"] = 'onap_helm_status'
        super(Status, self).__init__(**kwargs)
        self.__logger.debug("helm status init started")
        self.start_time = None
        self.stop_time = None
        self.result = 0
        self.helms = []
        self.details = {}
        self.res_dir = "{}/onap-helm".format(self.dir_results)

    def helm2_run(self):
        helm_ls = subprocess.Popen([HELM_BIN, 'ls', '--output', 'yaml'],
                stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        stdout,stderr = helm_ls.communicate()
        status = yaml.load(stdout, Loader=yaml.FullLoader)
        self.failing_helms = {}
        os.makedirs(self.res_dir, exist_ok=True)

        for release in status['Releases']:
            if ONAP_RELEASE in release['Name']:
                self.helms.append(release['Name'])
                if release['Status'] != "DEPLOYED":
                    helm_status = subprocess.Popen([
                        HELM_BIN,
                        'status',
                        release['Name'],
                        '--output',
                        'yaml'],
                        stdout=subprocess.PIPE,
                        stderr=subprocess.STDOUT)
                    stdout,stderr = helm_status.communicate()
                    release_status = yaml.load(stdout, Loader=yaml.FullLoader)
                    description = release_status['info']['Description']
                    failing_helm = {
                            'description': description,
                            'status': release['Status']}
                    if 'notes' in release_status['info']['status']:
                        details = release_status['info']['status']['notes']
                        failing_helm['details'] = details
                    self.failing_helms[release['Name']] = failing_helm
                    self.failing = True

    def helm3_run(self):
        run_env = os.environ.copy()
        helm_ls = subprocess.Popen([HELM_BIN, 'ls', '-A', '--output', 'yaml'],
                stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=run_env)
        stdout,stderr = helm_ls.communicate()
        status = yaml.load(stdout, Loader=yaml.FullLoader)
        self.failing_helms = {}
        os.makedirs(self.res_dir, exist_ok=True)

        for release in status:
            if ONAP_RELEASE in release['name']:
                self.helms.append(release['name'])
                if release['status'] != "deployed":
                    helm_status = subprocess.Popen([
                        HELM_BIN,
                        'status',
                        '--namespace',
                        release['namespace'],
                        release['name'],
                        '--output',
                        'yaml'],
                        stdout=subprocess.PIPE,
                        stderr=subprocess.STDOUT,
                        env=run_env)
                    stdout,stderr = helm_status.communicate()
                    release_status = yaml.load(stdout, Loader=yaml.FullLoader)
                    description = release_status['info']['description']
                    failing_helm = {
                            'description': description,
                            'status': release['status']}
                    if 'notes' in release_status['info']['status']:
                        details = release_status['info']['status']['notes']
                        failing_helm['details'] = details
                    self.failing_helms[release['name']] = failing_helm
                    self.failing = True

    def run(self):
        """Run tests."""
        self.start_time = time.time()
        self.__logger.debug("start test")
        self.failing = False
        helm_ls = subprocess.Popen(
            [HELM_BIN, 'version', '--template', '"{{.Version}}"'],
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
        )
        stdout,stderr = helm_ls.communicate()
        # Helm <3 will respond no value, Helm >=3 will answer with real version
        if '<no value>'.encode() in stdout:
            self.helm2_run()
        else:
            self.helm3_run()

        for file in glob.glob("{}/*.log".format(ONAP_HELM_LOG_PATH)):
            component = file.split('/')[-1].split('.')[0]
            copyfile(file,"{}/{}.log".format(self.res_dir, component))
            if component not in self.helms:
                data = ""
                with open(file, 'r') as content:
                    data = content.read()
                self.failing_helms[component] = {
                        'description': "helm deploy failure",
                        "details": data,
                        'status': 'FAILED'}
                self.failing = True
                self.helms.append(component)

        file = "{}/{}.log.log".format(ONAP_HELM_LOG_PATH, ONAP_RELEASE)
        component = file.split('/')[-1].split('.')[0]
        try:
            copyfile(file,"{}/{}.log".format(self.res_dir, component))
        except FileNotFoundError:
            self.__logger.error("file %s is not present, maybe path is not good",
                    file)

        self.jinja_env = Environment(autoescape=select_autoescape(['html']),
                                     loader=PackageLoader('helm_onap_status'))

        self.jinja_env.get_template(
            'index.html.j2').stream(ns=self).dump(
                '{}/helm.html'.format(self.res_dir))
        self.jinja_env.get_template(
            'raw_output.txt.j2').stream(ns=self,
                                        release=ONAP_RELEASE).dump(
                                            '{}/onap-helm.log'.format(
                                                self.res_dir))

        self.stop_time = time.time()
        self.details['helms'] = {
                'number': len(self.helms),
                'number_failing': len(self.failing_helms),
                'failing': list(self.failing_helms.keys())}
        if self.failing:
            self.__logger.error("helm onap status test failed.")
            self.__logger.error("number of errored helm: %s",
                                len(self.failing_helms))
            return testcase.TestCase.EX_TESTCASE_FAILED

        self.result = 100
        return testcase.TestCase.EX_OK
